#ifndef STACK_H
#define STACK_H

#include "../elementary.h"

/* Structs and Unions */

typedef struct stack {
    unsigned int length;     /* Max size */
    unsigned int count;      /* Number of elements */
    void *array[1]; /* Start of Stack */
} Stack;

/* Essential operations: Push and Pop */

Stack *stack_create(unsigned int size);

void stack_destroy(Stack *sp);

int stack_push(Stack *sp, void *sn);

void *stack_pop(Stack *sp);

/* Non-essential operations */

int stack_empty(Stack *sp);

int stack_full(Stack *sp);

#endif 

