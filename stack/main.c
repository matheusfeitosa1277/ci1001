#include "stack.h"
#include <stdio.h>
#include <stdlib.h> /* srandom e rand */
#include <time.h>

#define SEED 42
#define SIZE 67108864 /* 2^26 */

void *aux;

int main() {
    Stack *sp;
    unsigned int i = SIZE;
    clock_t start, end;

    start = clock();

    if (sp = stack_create(i)) {
        srandom(SEED);

        while (i--) {
            aux = rand() % SEED ;
            stack_push(sp, aux);
            //printf("%d%c", aux, i  ? ' ' : '\n');
        }

        stack_push(sp, aux); /* Pushing on a full Stack */


        while (!stack_empty(sp))
              stack_pop(sp);
            //printf("%d ", stack_pop(sp));

      //putchar('\n');

        stack_pop(sp); /* Popping on a empty Stack */

        stack_destroy(sp);
    }
    else
        puts("Could not create an stack");
    
    end = clock();

    printf("%f\n", ((double)end - start)/CLOCKS_PER_SEC);

    return 0;
}
