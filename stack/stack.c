#include "stack.h"
#include <stdlib.h>

/* Essential operations: Push and Pop */

Stack *stack_create(unsigned int size) {
    Stack *sp;
    
    if (sp =  malloc(sizeof (Stack) + sizeof(void *) * (size-1))) {
        sp->length = size;
        sp->count = 0;
    }

    return sp;
}

void stack_destroy(Stack *sp) {
    free(sp);
}

int stack_push(Stack *sp, void *sn) {
    if (sp->count < sp->length) { /* Stack not full */
        sp->array[sp->count++] = sn;
        return 0;
    }

    return 1;
}

void *stack_pop(Stack *sp) {
    if (sp->count) /* Stack not empty */
        return sp->array[--sp->count];

    return NULL;
}

/* Non-essential operations */

int stack_empty(Stack *sp) {
    return !sp->count;
}

int stack_full(Stack *sp) {
    return (sp->count == sp->length);
}

