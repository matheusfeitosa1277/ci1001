#ifndef ELEMENTARY_H
#define ELEMENTARY_H

typedef union elementary_node {
    char c;
    int n;
    float f;
    double d;
    void *p;
} ElementaryNode;

#endif

