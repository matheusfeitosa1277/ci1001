#include "xlist.h"
#include <stdlib.h>

/* Essential operations: Search, Inserte, Delete */

XList *xlist_create() {
    XList *lp;

    if (lp = (XList *) malloc(sizeof (XList))) 
        lp->xpointer =  NULL;

    return lp;
}

void xlist_destroy(XList *lp) {
    XListNode *next, *previous = NULL;

    do {
        next = (XListNode *) ((long)previous ^ (long)lp->xpointer);
        previous = lp->xpointer;
        free(lp);
        lp = next;
    } while (next);
}

/* Non-essential operations */

