#ifndef XLIST_H
#define XLIST_H

#include "../elementary.h"

/* Com essa interface, temos basicamente uma slist... */

/* Structs and Unions */

typedef struct xlist_node XList; /* 8 bytes Overhead */

typedef struct xlist_node {
    ElementaryNode data;
    struct xlist_node *xpointer;
} XListNode;

/* Essential operations: Search, Insert, Delete */

XList *xlist_create();

void xlist_destroy(XList *lp);

XListNode xlist_search(XList *lp, ElementaryNode ln);

int xlist_insert_head(XList *lp, ElementaryNode ln);

ElementaryNode xlist_delete(XList *lp, ElementaryNode ln);

/* Non-essential operations */

#endif

