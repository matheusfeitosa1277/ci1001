#include "dlist.h"
#include <stdlib.h>
#include <stdio.h>

/* Essential operations: Search, Insert, Remove */

DList *dlist_create() {
    DList *lp;

    if (lp = (DList *) malloc(sizeof (DList))) 
        lp->previous = lp->next = lp;

    return lp;
}

void dlist_destroy(DList *lp) {
    DListNode *aux;

    (*lp->previous).next = NULL;

    do {
        aux = lp; 
        lp = lp->next;
        free(aux);
    } while (lp);
}

DListNode *dlist_search(DList *lp, ElementaryNode ln) {
    DListNode *aux = lp;
    lp->data = ln; /* Sentinel */

    do 
        lp = lp->next;
    while (lp->data.p != ln.p);

    return (lp == aux ? NULL : lp);
}

ElementaryNode dlist_remove(DListNode *dln) {
    ElementaryNode aux;

    if (dln) {
        dln->previous->next = dln->next;
        dln->next->previous = dln->previous;
        aux = dln->data;
        free(dln);
        return aux;
    }

    return (ElementaryNode) NULL;
}

int dlist_insert_head(DList *lp, ElementaryNode ln) {
    DListNode *aux;

    if (aux = (DListNode *) malloc(sizeof (DListNode))) {
        aux->data = ln;
        aux->previous = lp;
        aux->next = lp->next;

        lp->next->previous = aux;
        lp->next = aux;
        return 0;
    }

    return 1;
}

/* Non-essential operations */

int dlist_insert_tail(DList *lp, ElementaryNode ln) {
    DListNode *aux;

    if (aux = (DListNode *) malloc(sizeof (DListNode))) {
        aux->data = ln;
        aux->next = lp;
        aux->previous = lp->previous;

        lp->previous->next = aux;
        lp->previous = aux;
        return 0;
    }

    return 1;
}

