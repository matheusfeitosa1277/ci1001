#ifndef DLIST_H
#define DLIST_H

#include "../elementary.h"

/* lista circular duplamente ligada com uma sentinela */

/* Structs and Unions */

typedef struct dlist_node DList;

typedef struct dlist_node {
    ElementaryNode data;
    struct dlist_node *previous;
    struct dlist_node *next;
} DListNode;

/* Essential operations: Search, Insert, Remove */

DList *dlist_create();

void dlist_destroy(DList *lp);

DListNode *dlist_search(DList *lp, ElementaryNode ln);

ElementaryNode dlist_remove(DListNode *dln);

int dlist_insert_head(DList *lp, ElementaryNode ln);

/* Non-essential operations */

int dlist_insert_tail(DList *lp, ElementaryNode ln);

//int dlist_inser_before(DList *lp, ElementaryNode ln, DListNode *dln)

//int dlist_inser_after(DList *lp, ElementaryNode ln, DListNode *dln)

#endif

