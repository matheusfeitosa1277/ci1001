#include "dlist.h"
#include "../stailq/stailq.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SEED 42
#define SIZE 16777216 /* 2^24 */

ElementaryNode aux;

int main() {

    STailq *qp;
    DList *lp;
    unsigned int i = SIZE;
    clock_t start, end;

    start = clock();

    qp = stailq_create();
    lp = dlist_create();

    srandom(SEED);

    while (i--) {
        aux.n = rand() % SEED;
        stailq_enqueue(qp, aux);
        dlist_insert_tail(lp, aux);
      //printf("%d%c", aux.n, i ? ' ' : '\n');
    }

    while (dlist_remove(dlist_search(lp, stailq_dequeue(qp))).n);
    /*
    while (!stailq_empty(qp))
        printf("%2d ", dlist_remove(dlist_search(lp, stailq_dequeue(qp))).n);
    
    putchar('\n');
    */

    stailq_destroy(qp);

    dlist_destroy(lp);

    end = clock();

    printf("%f\n", ((double)end - start)/CLOCKS_PER_SEC);

    return 0;
}

