#ifndef STAILQ_H
#define STAILQ_H

#include "../elementary.h"

/* Structs and Unions */

typedef struct stailq_node {
    ElementaryNode data;
    struct stailq_node *next;
} STailqNode;

typedef struct stailq {
    STailqNode head;  /* Sentinel Start *//* 8 bytes Overhead */
    STailqNode *tail; /* End of stailq */
} STailq;

/* Essential operations: Queue and Dequeue */

STailq *stailq_create();

void stailq_destroy(STailq *qp);

int stailq_enqueue(STailq *qp, ElementaryNode qn);

ElementaryNode stailq_dequeue(STailq *qp);

/* Non-essential operations */

int stailq_empty(STailq *qp);

#endif

