#include "stailq.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SEED 42
#define SIZE 33554432 /* 2^25 */

ElementaryNode aux;

int main() {
    STailq *qp;
    unsigned int i = SIZE;
    clock_t start, end;

    start = clock();

    qp = stailq_create();
    srandom(SEED);

    while (i--) {
        aux.n = rand() % SEED;
        stailq_enqueue(qp, aux);
      //printf("%d%c", aux.n, i ? ' ' : '\n');
    }

    while (stailq_dequeue(qp).p);

  //while (!stailq_empty(qp)) 
    //printf("%d ", stailq_dequeue(qp).n);

  //putchar('\n');

    stailq_dequeue(qp); /* Dequeue on a empty STailq */

    stailq_destroy(qp);

    end = clock();

    printf("%f\n", ((double)end - start)/CLOCKS_PER_SEC);

    return 0;
}

