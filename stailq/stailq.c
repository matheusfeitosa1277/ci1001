#include "stailq.h"
#include <stdlib.h>

/* Essential operations: Queue and Dequeue */

STailq *stailq_create() {
    STailq *qp;

    if (qp = (STailq *) malloc(sizeof (STailq))) {
        qp->tail = &qp->head;
        qp->head.next = NULL;
    }

    return qp;
}

void stailq_destroy(STailq *qp) {
    STailqNode *aux;

    while (qp->head.next) { /* STailq not empty */
        aux = qp->head.next;
        qp->head.next = qp->head.next->next;
        free(aux);
    }

   free(qp);
}

int stailq_enqueue(STailq *qp, ElementaryNode qn) {
    if (qp->tail->next = (STailqNode *) malloc(sizeof (STailqNode))) {
        qp->tail->next->data = qn;
        qp->tail->next->next = NULL;
        qp->tail = qp->tail->next;
        return 0;
    }

    return 1;
}

ElementaryNode stailq_dequeue(STailq *qp) {
    ElementaryNode key;
    STailqNode *aux;

    if (qp->head.next) { /* STailq not empty */
        aux = qp->head.next;
        key = qp->head.next->data;
        qp->head.next = qp->head.next->next;
        free(aux);
        return key;
    }

    return (ElementaryNode) NULL;
}

/* Non-essential operations */

int stailq_empty(STailq *qp) {
    return !qp->head.next;
}

